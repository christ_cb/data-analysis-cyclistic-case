---
editor_options: 
  markdown: 
    wrap: 72
---

# Data Analysis Case Study - Cyclistic Bike-share

## Overview

This repository contains the data analysis case study I conducted to
showcase my skills in data preprocessing, exploratory data analysis,
feature engineering, modeling, and visualization using R. The goal of
this analysis was to answer the following question asked by the
Marketing Director: "How do annual members and casual riders use
Cyclistic bikes differently?". This with the objective of creating a new
marketing strategy that can help casual users to become annual users.

## Table of Contents

-   [Introduction](#introduction)
-   [Data Cleaning and Preprocessing](#data-cleaning-and-preprocessing)
-   [Exploratory Data Analysis (EDA)](#exploratory-data-analysis)
-   [Feature Engineering](#feature-engineering)
-   [Visualization of Results](#visualization-of-results)
-   [Business Impact Analysis](#business-impact-analysis)
-   [Lessons Learned](#lessons-learned)
-   [How to Run the Code](#how-to-run-the-code)
-   [Contact Information](#contact-information)

## Introduction {#introduction}

In this case study, I analyzed the trip data of users for the year 2023
provided by [Divvy
Bikes](https://divvy-tripdata.s3.amazonaws.com/index.html "Dataset index").
The main objectives were to:

1\. [Clean the data with the use of
R.](#data-cleaning-and-preprocessing)

2\. [Find trends that can assist the marketing department to create a
new strategy to turn casual users into annual
users.](#exploratory-data-analysis)

3\. [Provide some strategic recommendations.](#business-impact-analysis)

## Data Cleaning and Preprocessing {#data-cleaning-and-preprocessing}

This specific dataset required some cleaning and preprocessing to ensure
quality and consistency, these were some of the steps taken:

-   Handling missing values

-   Removing duplicates or inconsistent data

-   Standardizing data formats

You can find the detailed pre-processing script in
[`Data_Analysis.R`](Data_analysis/RMarkdown_Cyclist.Rmd).

## Exploratory Data Analysis (EDA) {#exploratory-data-analysis}

I conducted extensive exploratory data analysis to understand the
dataset better. This involved:

-   Generating summary statistics

-   Creating visualizations to identify patterns and trends

-   Detecting and addressing outliers

The EDA script is available in
[`Data_Analysis.R`](Data_analysis/RMarkdown_Cyclist.Rmd).

## Feature Engineering {#feature-engineering}

Based on the insights from EDA, I engineered several features to improve
model performance. This included:

-   Creating new variables from existing data.

-   Transforming variables to better capture underlying patterns.

Details are in [`Data_Analysis.R`](Data_analysis/RMarkdown_Cyclist.Rmd).

## Visualization of Results {#visualization-of-results}

In order to communicate my findings in the most adequate way, I created
several visualizations. These include:

-   Bar chart for casual riders and annual members.

-   Rides per day for casual riders and members.

-   Rides per month for casual riders and members.

Visualizations are generated in
[`Data_Analysis.R`](Data_analysis/RMarkdown_Cyclist.Rmd).

## Business Impact Analysis {#business-impact-analysis}

The analysis provided actionable insights that could lead to:

-   Launch new marketing campaigns.

-   Change price model.

-   Company grow, based on the analysis made by the marketing team.

Details on the business impact are discussed in the ['final
presentation'](Final_report/Cyclistic_Case_Analysis_Stakeholders.pdf).

## Lessons Learned {#lessons-learned}

Throughout this project, I learned valuable lessons including:

-   Always review data before begin processing it.

-   You can support your analysis with many tools, not just the one
    originally selected. This will make your job easier.

-   There is a lot to learn in data analysis, like the use of machine
    learning models to help solve the issues.

## How to Run the Code {#how-to-run-the-code}

To replicate this analysis, follow these steps:

1.  Download the raw data from the URL:
    <https://divvy-tripdata.s3.amazonaws.com/index.html> and store it in
    the same folder where you will download the data from the
    repository. I used all year of 2023.
2.  Download all the info from the repository, either as ZIP or with git
    directly and use the same directory as step 1.
3.  Start RStudio and change the working directory to the one used in
    the previous steps with the command setwd("Directory address"). For
    example: setwd("/home/christian/case1")
4.  Edit the RMarkdown file located in
    Data_analysis/RMarkdown_Cyclist.Rmd with your working directory and
    then execute it step by step to see the results one by one.

## Contact Information {#contact-information}

If you have any questions or would like to discuss this project further,
please feel free to contact me:

Name: [Christian Cuadra]

Email: [[christ_cb\@hotmail.com](mailto:christ_cb@hotmail.com){.email}]

LinkedIn: [<https://www.linkedin.com/in/christian-c-723a7a245/>]

Thank you for reviewing my analysis case study!
